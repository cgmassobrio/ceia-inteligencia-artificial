import numpy as np
from ejercicio_1 import Norm
from ejercicio_2 import sorting_vector_norm_l2

x = np.array([[1,1,1,1,0],
              [4,0,0,3,0],
              [2,1,1,3,1]])
l = Norm(x)
print(f"Norma L2 = {Norm.vector_norm_l2(l)}")
print(f"Matriz x reordenada = \n {sorting_vector_norm_l2(x)}")