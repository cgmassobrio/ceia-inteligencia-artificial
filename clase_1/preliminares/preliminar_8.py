import numpy as np
from ejercicio_8 import K_means

#Variable para generar random
rng = np.random.default_rng(seed=18)

#Datos y parámetros
data = rng.integers(10, size=(7,5), dtype=np.uint8) #Matriz de datos aleatorios
clusters = 3
iteraciones = 10

resultados = K_means(data,clusters,iteraciones)
print(f"Indice de filas centroides con menor distancia euclidea a filas de data = \n {resultados[0]}")
print(f"Matriz final de centroides = \n {resultados[1]}")