import numpy as np
from ejercicio_7 import min_dist_centroids

X = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
C = np.array([[1, 0, 0], [0, 1, 1]])

print(f"Indice de filas C con menor distancia euclidea a filas de X = {min_dist_centroids(X,C)}")