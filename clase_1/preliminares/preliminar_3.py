import numpy as np
from ejercicio_3 import Indexer

users_id = np.array([15, 12, 14, 10, 1, 2, 1])
ids = Indexer(users_id)
print(f"Identificadores de usuarios: users_id = {[15, 12, 14, 10, 1, 2, 1]}")
print(f" id2idx = {Indexer.get_idxs(ids)}")
print(f" idx2id = {Indexer.get_ids(ids)}")

