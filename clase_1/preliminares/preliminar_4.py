import numpy as np
from ejercicio_4 import Metric

truth = [1,1,0,1,1,1,0,0,0,1]
prediction = [1,1,1,1,0,0,1,1,0,0]
truth = np.array(truth)
prediction = np.array(prediction)


metricas = Metric(prediction,truth)
print(f"Precision: {Metric.precision(metricas)}")
print(f"Recall: {Metric.recall(metricas)}")
print(f"Accuracy: {Metric.accuracy(metricas)}")

