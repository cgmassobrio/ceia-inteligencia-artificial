import numpy as np
from ejercicio_5 import QueryMeanPrecision

q_id = np.array([1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 4])
truth_relev = np.array([1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1])

qmp = QueryMeanPrecision(q_id,truth_relev)
print(f"average query precision: {QueryMeanPrecision.qmp(qmp)}")