import numpy as np
from ejercicio_1 import Norm

rng = np.random.default_rng(seed=18)

x = np.array([[1,1,1,1,0],[4,0,0,3,0],[2,1,1,3,1]])
l = Norm(x)

print(f"Matriz = {x}")
print(f"Norma L0 = {Norm.vector_norm_l0(l)}")
print(f"Norma L1 = {Norm.vector_norm_l1(l)}")
print(f"Norma L2 = {Norm.vector_norm_l2(l)}")
print(f"Norma inf = {Norm.vector_norm_linf(l)}")


