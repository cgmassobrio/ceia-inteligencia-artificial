import numpy as np
from ejercicio_6 import diference

X = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
C = np.array([[1, 0, 0], [0, 1, 1]])

diferencia = diference(X,C)
print(f"Distancia de X a C = \n {diferencia}")