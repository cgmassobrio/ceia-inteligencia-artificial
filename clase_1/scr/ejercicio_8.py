import numpy as np

def K_means(data,n,iterations):
    '''
    :param data: matriz de datos
    :param n: contidad de clusters
    :param iterations: número de iteraciones
    :return:
    '''
    # indexado de las filas de las matriz data para luego emplearlas en centroids
    idx = np.array([x for x in range(0, len(data), 1)])
    # Se define la primer matriz de centroides eligiendo al azar filas de la matriz data
    centroids = data[np.random.choice(idx, size=n, replace=False)]

    for i in range(iterations): #ejecucion de iteraciones
        # Cálculo de distancia de cada vector a centroide
        distance = np.sqrt(np.sum((data - centroids[:,None]) ** 2, axis=2))
        # Clasificación de los índices de las filas la matriz de centroides con menor distancia a las filas de datos
        clasification = np.argmin(distance, axis=0)
        for c in range(n): #actualización de cada centroide
            #Se define el punto medio entre filas de datos cercanas a un determinado centroide
            centroids[c] = np.mean(data[clasification == c,:],axis=0)

    #Reacomodamiento de resultados
    distance = np.sqrt(np.sum((data - centroids[:,None]) ** 2, axis=2))
    clasification = np.argmin(distance, axis=0)
    centroids = np.reshape(centroids,(n,data.shape[1]))
    return clasification, centroids



    #

