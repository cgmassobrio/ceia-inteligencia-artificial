class Metric:
    def __init__(self, prediction, truth):
        self.prediction = prediction
        self.truth = truth

    def precision(self):
        prediction = self.prediction
        truth = self.truth
        true_pos = ((truth == 1) & (prediction == 1)).sum()
        false_pos = ((truth == 1) & (prediction == 0)).sum()
        return true_pos/(true_pos+false_pos)

    def recall(self):
        prediction = self.prediction
        truth = self.truth
        true_pos = ((truth == 1) & (prediction == 1)).sum()
        false_neg = ((truth == 0) & (prediction == 1)).sum()
        return true_pos/(true_pos+false_neg)

    def accuracy(self):
        prediction = self.prediction
        truth = self.truth
        true_pos = ((truth == 1) & (prediction == 1)).sum()
        true_neg = ((truth == 0) & (prediction == 0)).sum()
        false_pos = ((truth == 1) & (prediction == 0)).sum()
        false_neg = ((truth == 0) & (prediction == 1)).sum()
        return (true_pos+true_neg)/(true_pos+true_neg+false_pos+false_neg)
