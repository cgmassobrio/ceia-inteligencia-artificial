import numpy as np

class Indexer:
    def __init__(self,users_id):
        unique_id, unique_id_idx = np.unique(users_id, return_index=True)
        unique_id_rev = unique_id[unique_id_idx.argsort()]
        id2idx = np.ones(unique_id_rev.max() + 1, dtype=np.int8) * -1
        id2idx[unique_id_rev] = np.arange(unique_id_rev.size)
        self.users_id = users_id
        self.id2idx = id2idx
        self.idx2id = unique_id_rev

    def get_idxs(self):
        return self.id2idx[self.users_id]

    def get_ids(self):
        idx = self.id2idx[self.users_id]
        return self.idx2id[idx]
