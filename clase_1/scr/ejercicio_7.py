import numpy as np
from ejercicio_6 import diference

def min_dist_centroids(data,centroids):
    #Cálculo de distancia de los vectores fila de la matriz de datos a los centroides
    diference_mask = diference(data,centroids)
    distance = np.sqrt(np.sum(diference_mask**2,axis=2))
    #Clasificación de los índices de las filas la matriz de centroides con menor distancia a las filas de datos
    return np.argmin(distance,axis=0)


