import numpy as np

class Norm:
    def __init__(self,matrix):
        self.matrix = matrix

    # Cálculo de las distintas normas de los vectores filas
    def vector_norm_l0(self):
        return np.sum(self.matrix != 0, axis=1)

    def vector_norm_l1(self):
        return np.sum(abs(self.matrix),axis=1,dtype=np.uint8)

    def vector_norm_l2(self):
        return np.sqrt(np.sum(self.matrix**2,axis=1))

    def vector_norm_linf(self):
        return np.max(self.matrix,axis=1)
