import numpy as np

class QueryMeanPrecision:
    def __init__(self,q_id,truth_relev):
        self.q_id = q_id
        self.truth_relev = truth_relev

    def qmp(self):
        q_id = self.q_id
        truth_relev = self.truth_relev
        #Documentos verdaderos
        q_id_true = q_id[truth_relev == 1]
        # Cantidad de documentos relevantes por q_id
        q_id_true_count = np.bincount(q_id_true)
        # Contar documentos de acuerdo a relevancia
        unique_q_id = np.unique(q_id)
        true_relev_count = q_id_true_count[unique_q_id]
        # Total de documentos
        total_documents = np.bincount(q_id)[unique_q_id]
        # Cálculo de la métrica
        precision_by_q = true_relev_count / total_documents
        return np.mean(precision_by_q)

