import numpy as np
from ejercicio_1 import Norm

def sorting_vector_norm_l2(matrix):
    M = Norm(matrix)
    n_l2 = Norm.vector_norm_l2(M)
    arg_sort = np.argsort(n_l2 * -1)
    return matrix[arg_sort,:]
